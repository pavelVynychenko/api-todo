'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addIndex('Users', ['email'], {
      name: 'user_email',
      unique: true
     });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeIndex('Users', '`user_email`');
  }
};