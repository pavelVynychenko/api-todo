module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.bulkInsert(
      'Users',
      [
        {
          name: 'admin',
          email: 'user',
          password: '123',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          name: 'Angela Merkel',
          email: 'AngMerk@example.com',
          password: '123456',
          createdAt: new Date(),
          updatedAt: new Date(),
        },
      ],
      {},
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete('Users', null, {})
};