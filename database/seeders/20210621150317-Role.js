module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Roles",
      [
        {
          roleName: "admin",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { 
          roleName: "user",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { 
          roleName: "moderator",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],

      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("Roles", null, {})
};