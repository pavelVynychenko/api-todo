module.exports = {
  up: (queryInterface, Sequelize) =>
    queryInterface.bulkInsert(
      "Deals",
      [
        {
          userId: 1,
          title: "Do sometning with your life",
          content:
            "Nulla mollis molestie lorem. Quisque ut erat. Curabitur gravida nisi at nibh.",
          status: "todo",
          priority: "high",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { 
          userId: 1,
          title: 'clean up your room',
          content:
            "Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.",
          status: "doing",
          priority: "low",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { 
          userId: 1,
          title: 'task 3',
          content:
            "Maecenas tincidunt lacus at velit. Vivamus vel nulla eget eros elementum pellentesque. Quisque porta volutpat erat.",
          status: "done",
          priority: "medium",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],

      {}
    ),

  down: (queryInterface, Sequelize) =>
    queryInterface.bulkDelete("Deals", null, {})
};