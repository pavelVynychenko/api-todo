'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    queryInterface.bulkInsert(
      "Posts",
      [
        {
          userId: 1,
          title: "Some nice title",
          content:
            "Lorem nice content.",
          createdAt: new Date(),
          updatedAt: new Date()
        },
        { 
          userId: 2,
          title: 'Some dummy title',
          content:
            "Lorem bla bla bla bla nice content",
          createdAt: new Date(),
          updatedAt: new Date()
        }
      ],
      {}
    )
  },

  down: (queryInterface, Sequelize) => 
    queryInterface.bulkDelete("Posts", null, {})
};
