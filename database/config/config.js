require('dotenv').config()

module.exports = {
  development: {
    username: process.env.DATABASE_USER,
    password: process.env.DATABASE_PASSWORD,
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    database: process.env.DATABASE_NAME,
    dialect: 'mysql',
    logging: console.log
  },
  production: {
    url: process.env.DATABASE_URL,
    dialect: 'mysql'
  },
}
