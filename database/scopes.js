const compact = require('lodash/compact');
const camelCase = require('lodash/camelCase');
const {
  Op, fn, col, literal
}           = require('sequelize');

module.exports = (dbClient) => [
  [ 'byId',
    (id) => ({
      where: { id }
    })
  ],
  [ 'paginable',
    (limit = Number.MAX_SAFE_INTEGER, offset = 0) => ({ limit: +limit, offset: +offset })
  ],
  [ 'searchable',
    (searchField, searchValue) => ({
      where: {
        [ searchField ]: { [ Op.like ]: `%${ searchValue }%` }
      }
    })
  ],
  [ 'notLike',
    (field, value) => ({
      where: {
        [ field ]: { [ Op.notLike ]: `%${ value }%` }
      }
    })
  ],
  [ 'ordered',
    (orderBy, dir) => ({
      order: [ [ col(orderBy), dir ] ]
    })
  ],
  [ 'byKeyInArray',
    (key, array) => ({
      where: {
        [ key ]: {
          [ Op.in ]: compact(array)
        }
      }
    })
  ],
  [ 'isBetween',
    (key, start, end) => ({
      where: {
        [ key ]: {
          [ Op.and ]: {
            [ Op.gte ]: start,
            [ Op.lte ]: end
          }
        }
      }
    })
  ],
  [ 'byOrCondition',
    (array) => ({
      where: {
        [ Op.or ]: array
      }
    })
  ],
  [ 'byGreaterEqualCondition',
    (key, value) => ({
      where: {
        [ key ]: {
          [ Op.gte ]: value
        }
      }
    })
  ],
  [ 'byLeatherEqualCondition',
    (key, value) => ({
      where: {
        [ key ]: {
          [ Op.lte ]: value
        }
      }
    })
  ],
  [ 'byKeyIsNotInArray',
    (key, array) => ({
      where: {
        [ key ]: {
          [ Op.notIn ]: array
        }
      }
    })
  ],
  [ 'byKeyNotequal',
    (key, value) => ({
      where: {
        [ key ]: {
          [ Op.ne ]: value
        }
      }
    })
  ],
  [ 'countGroupBy', function countGroupBy(group, attributes) {
    const query = {
      attributes: attributes.map(field => [ fn('count', col(field)), field === '*' ? 'count' : field ])
    };
    group       && Object.assign(query, {
      group:      group.map(g => col(g)),
      attributes: [
        ...query.attributes,
        ...group.filter(g => Object.keys(dbClient[ camelCase(this.name) ].rawAttributes).includes(g))
          .map(g => [ col(g), g ])
      ]
    });
    return query;
  }
  ], [
    'excludeKeys', (exclude) => ({
      attributes: {
        exclude: exclude.map(e => col(e))
      }
    })
  ],
  [ 'isNull',
    (field) => ({
      where: {
        [ field ]: {
          [ Op.is ]: null
        }
      }
    })
  ],
  [ 'isNotNull',
    (field) => ({
      where: {
        [ field ]: {
          [ Op.not ]: null
        }
      }
    })
  ],
  [ 'extractDate', (what, from, alias) => ({
    attributes: [
      [ literal(`extract(${ what } from ${ from })`), alias ]
    ]
  })  ],
  [ 'dateFormat', (what, format, alias) => ({
    attributes: [
      [ fn('DATE_FORMAT', col(what), format), alias ]
    ]
  })  ],
  [ 'attribute', (ref, alias) => ({
    attributes: [
      [ col(ref), alias || ref ]
    ]
  })  ],
  [ 'fullAttributes', function fullAttributes() {
    return {
      attributes: Object.keys(dbClient[ camelCase(this.name) ].rawAttributes)
    };
  }  ],
  [ 'havingGt', (key, value) => ({
    having: {
      [ key ]: {
        [ Op.gt ]: value
      }
    }
  })  ],
  [ 'sumGroupBy', function sumGroupBy(group, attributes) {
    const foo = (str) => {
      console.log(str);
      if ((str || '').split('.').length > 1) {
        return str;
      }
      return `${ this.name }.${ str }`;
    };
    const groupField =  (group || []).map(g => col(g));
    const query      = {
      attributes: attributes.map(field => [
        fn('sum', col(`${ foo(field.col || field) }`)), field.name || field
      ])
    };
    group            && Object.assign(query, {
      group:      groupField,
      attributes: [
        ...query.attributes,
        ...group
          .filter(g => Object.keys(dbClient[ camelCase(this.name) ].rawAttributes).includes(g))
          .map(g => [ col(g), g ])
      ]
    });
    return query;
  }  ]
];

