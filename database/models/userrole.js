const { Model, DataTypes } = require('sequelize');

class UserRole extends Model {
  static init(sequelize) {
    
    return super.init({
      userId: DataTypes.INTEGER,
      roleId: DataTypes.INTEGER
    }, { sequelize, modelName: 'UserRole' })
  }

  static associate(models) {}
};

module.exports = UserRole;