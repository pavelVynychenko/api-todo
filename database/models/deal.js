const { Model, DataTypes } = require('sequelize');

class Deal extends Model {
  static init(sequelize) {
    return super.init({
      title: DataTypes.STRING,
      content: DataTypes.TEXT,
      status: DataTypes.STRING,
      priority: DataTypes.STRING,
      userId: DataTypes.INTEGER
    }, { sequelize, modelName: 'Deal' })
  }

  static associate(models) {
    Deal.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'author',
      onDelete: 'CASCADE',
    });
  }
}

module.exports = Deal;
