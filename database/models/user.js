const { Model, DataTypes } =      require('sequelize');
const bcrypt =                    require("bcrypt");

class User extends Model {
  static init(sequelize) {
    return super.init({
      name: DataTypes.STRING,
      email: DataTypes.STRING,
      password: DataTypes.STRING
    }, { sequelize, modelName: 'User' })
  }

  static associate(models) {
    User.hasMany(models.Deal, {
      foreignKey: 'userId',
      as: 'deals',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.Post, {
      foreignKey: 'userId',
      as: 'posts',
      onDelete: 'CASCADE',
    });
    User.hasMany(models.Comment, {
      foreignKey: 'userId',
      as: 'comments',
      onDelete: 'CASCADE',
    });
    User.belongsToMany(models.Role, {
      through: 'UserRole',
      as: 'roles',
      foreignKey: 'userId',
    });
  };

  passwordCheck(bodyPass) {
    const isPassValid = bcrypt.compareSync(bodyPass, this.password)

    if (isPassValid) return
    
    throw new Error('invalid password');
  }
}

module.exports = User;
