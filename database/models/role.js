const { Model, DataTypes } =      require('sequelize');

class Role extends Model {
  static init(sequelize) {
    
    return super.init({
      roleName: DataTypes.STRING
    }, { sequelize, modelName: 'Role' })
  }

  static associate(models) {
    Role.belongsToMany(models.User, {
      through: 'UserRole',
      as: 'users',
      foreignKey: 'roleId'
    });
  }
};

module.exports = Role;
