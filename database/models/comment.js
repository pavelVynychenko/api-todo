const { Model, DataTypes } = require('sequelize');

class Comment extends Model {
  static init(sequelize) {
    return super.init({
      postId: DataTypes.INTEGER,
      comment: DataTypes.TEXT,
      userId: DataTypes.INTEGER
    }, { sequelize, modelName: 'Comment' })
  }

  static associate(models) {
    Comment.belongsTo(models.User, {
      foreignKey: 'userId',
      as: 'author',
    });
    Comment.belongsTo(models.Post, {
      foreignKey: 'postId',
      as: 'post'
    });
  }
}

module.exports = Comment;
