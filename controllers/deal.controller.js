const DealService = require('../services/deal.service');
const { celebrate, Joi } = require('celebrate')

class DealController {
    validationList = {
        status: ['todo', 'doing', 'done', 'archived'],
        priority: ['low', 'medium', 'high']
    }

    get preCreate() {
        return celebrate({
            body: Joi.object().keys({
                title: Joi.string().required(),
                content: Joi.string().allow(null, ''),
                status: Joi.string().valid('todo', 'doing', 'done', 'archived'),
                priority: Joi.string().valid('low', 'medium', 'high'),
                userId: Joi.number().required(),
            })
        });
    }

    async create(req, res) {
        const deal = await DealService.create(req.body)
        res.status(201).send(deal)
    }

    get preGetAll() {
        return celebrate({
            query: Joi.object().keys({
                title: Joi.string().allow(''),
                status: Joi.string().allow('', 'todo', 'doing', 'done', 'archived')
            })
        });
    }
    //TODO needs refactoring
    async getAll(req, res) {
        const userId = req.user.id
        const filters = req.query
        const pagination = {
            limit: req.query.limit,
            offset: req.query.offset
        }
        const deals = await DealService.getAll(userId, filters, pagination)
        res.json(deals)
    }

    get preGetOne() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }
    
    async getOne(req, res) {
        const deal = await DealService.getOne(req.params.id)
        res.status(200).send(deal)
    }

    get preUpdate() {
        return celebrate({
            body: Joi.object().keys({
                id: Joi.number().required(),
                title: Joi.string().required(),
                content: Joi.string().required(),
                status: Joi.string().valid('todo', 'doing', 'done', 'archived'),
                priority: Joi.string().valid('low', 'medium', 'high'),
            })
        });
    }

    async update(req, res) {
        const updatedDeal = await DealService.update(req.body)
        res.send({
            message: `deal '${req.body.id}' was updated successfully.`,
            data: updatedDeal
        });
    }

    get preDelete() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }

    async delete(req, res) {
        await DealService.delete(req.params.id)
        res.send({
            data: {
                id: req.params.id
            },
            message: `deal '${req.params.id}' was deleted successfully.`
        });
    }
}

module.exports = new DealController()