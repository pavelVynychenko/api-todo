const UserService = require('../services/user.service');
const { celebrate, Joi } = require('celebrate');

class UserController {
    
    async getAll(req, res) {
        const users = await UserService.getAll()
        res.json(users)
    }

    get preGetOne() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }
    
    async getOne(req, res) {
        const userId = req.params.id
        const user = await UserService.getOne(userId)
        res.status(200).send(user)
    }

    get preUpdate() {
        return celebrate({
            body: Joi.object().keys({
                id: Joi.number().required(),
                name: Joi.string().required(),
                email: Joi.string().required(),
                password: Joi.string()
            })
        });
    }

    async update(req, res) {
        const {id, name, email, password} = req.body
        await UserService.update(id, name, email, password)
        res.send({
            message: `user '${id}' was updated successfully.`
        });
    }

    get preDelete() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }

    async delete(req, res) {
        const userId = req.params.id
        await UserService.delete(userId)
        res.send({
            message: `user '${userId}' was deleted successfully.`
        });
    }
}

module.exports = new UserController()