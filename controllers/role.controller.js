const RoleService = require('../services/role.service');
const UserService = require('../services/user.service');
const { celebrate, Joi } = require('celebrate');

class RoleController {
    
    async getAll(req, res) {
        const roles = await RoleService.getAll()
        res.status(200).send(roles)
    }

    get preGetOne() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }
    
    async getOne(req, res) {
        const roleId = req.params.id
        const role = await RoleService.getOne(roleId)
        if (!role) {
            return res.status(404).send({
                message: 'Role Not Found',
            });
        }
        return res.status(200).send(role);
    }

    get preCreate() {
        return celebrate({
            body: Joi.object().keys({
                roleName: Joi.string().required(),
            })
        });
    }

    async create(req, res) {
        const {roleName} = req.body
        //check if this role already exists
        const isRoleExists = await RoleService.isRoleExists(roleName)

        if(isRoleExists) {
            return res.status(400).json({message: `Role '${roleName}' already exist`})
        }
        const role = await RoleService.create(roleName)
        res.status(201).send(role)
    }

    get preAddUser() {
        return celebrate({
            body: Joi.object().keys({
                userId: Joi.number().required(),
                roleId: Joi.number().required()
                
            })
        });
    }

    async addUser(req, res) {
        const userId = req.body.userId
        const roleId = req.body.roleId
        const role = await RoleService.findRole(roleId)

        if (!role) {
            return res.status(404).send({
              message: 'Role Not Found',
            });
        }
        const user = await UserService.getOne(userId)
        if (!user) {
            return res.status(404).send({
              message: 'User Not Found',
            });
        }
        await role.addUser(user); // !todo move to service? 🤔
        return res.status(200).send(role);
    }

    get preUpdate() {
        return celebrate({
            body: Joi.object().keys({
                roleName: Joi.string().required(),
            }),
            params: Joi.object().required().keys({
                id: Joi.number().required(),
            }),
        });
    }

    async update(req, res) {
        const roleName = req.body.roleName
        const id = req.params.id

        await RoleService.update(id, roleName)
        res.send({
            message: `role '${id}' was updated successfully.`
        });
    }

    get preDelete() {
        return celebrate({
          params: Joi.object().required().keys({
            id: Joi.number().required(),
          }),
        });
    }

    async delete(req, res) {
        const id = req.params.id
        await RoleService.delete(id)
        res.send({
            message: `role '${id}' was deleted successfully.`
        });
    }
}

module.exports = new RoleController()