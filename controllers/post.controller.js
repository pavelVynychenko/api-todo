const PostService = require('../services/post.service');

class PostController {
    async getAll(req, res) {
        const posts = await PostService.getAll()
        res.json(posts)
    }
}

module.exports = new PostController()