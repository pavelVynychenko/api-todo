const AuthService = require('../services/auth.service');
const { celebrate, Joi } = require('celebrate');

class AuthController {

    get preCreate() {
        return celebrate({
            body: Joi.object().keys({
                name: Joi.string().required(),
                email: Joi.string().required(),
                password: Joi.string().required(),
                roles: Joi.array().items(Joi.string().valid("user", "moderator", "admin")),
            })
        });
    }

    async create(req, res) {
        const {name, email, password, roles} = req.body
        const user = await AuthService.registration({name, email, password, roles})
        res.status(201).send(user)
    }

    async auth(req, res) {
        const user = await AuthService.auth(req.user.id)
        return res.json({user})
    }

    get preLogin() {
        return celebrate({
            body: Joi.object().keys({
                email: Joi.string().required(),
                password: Joi.string().required(),
            })
        });
    }

    async login(req, res) {
        const {email, password} = req.body
        const token = await AuthService.login(email, password)
        res.json(token)
    }
}

module.exports = new AuthController()