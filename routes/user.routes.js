const Router =              require('express')
const asyncHandler =        require('express-async-handler')
const userController =      require('../controllers/user.controller')

const router = new Router()

router.get('/users',  asyncHandler(userController.getAll))
router.get('/user/:id', [userController.preGetOne], asyncHandler(userController.getOne))
router.patch('/user', [userController.preUpdate], asyncHandler(userController.update))
router.delete('/user/:id', [userController.preDelete], asyncHandler(userController.delete))

module.exports = router
