const Router =              require('express')
const asyncHandler =        require('express-async-handler')
const postController =      require('../controllers/post.controller')

const router = new Router()

router.get('/posts', asyncHandler(postController.getAll))

module.exports = router
