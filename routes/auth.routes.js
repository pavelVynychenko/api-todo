const Router =              require('express')
const asyncHandler =        require('express-async-handler')
const authController =      require('../controllers/auth.controller')
const authMiddleware =      require('../middleware/auth.middleware')

const router = new Router()

router.post('/registration', [authController.preCreate], asyncHandler(authController.create))
router.post('/login', [authController.preLogin], asyncHandler(authController.login))
router.get('/auth', authMiddleware, asyncHandler(authController.auth))

module.exports = router
