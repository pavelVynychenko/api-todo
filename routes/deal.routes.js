const Router =              require('express')
const asyncHandler =        require('express-async-handler')
const dealController =      require('../controllers/deal.controller')
const authMiddleware =      require('../middleware/auth.middleware')

const router = new Router()

router.post('/deal', [authMiddleware, dealController.preCreate], asyncHandler(dealController.create))
router.get('/deals', [authMiddleware, dealController.preGetAll], asyncHandler(dealController.getAll))
router.get('/deal/:id', [authMiddleware, dealController.preGetOne], asyncHandler(dealController.getOne))
router.patch('/deal', [authMiddleware, dealController.preUpdate], asyncHandler(dealController.update))
router.delete('/deal/:id', [authMiddleware, dealController.preDelete], asyncHandler(dealController.delete))

module.exports = router
