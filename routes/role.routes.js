const Router =              require('express')
const asyncHandler =        require('express-async-handler')
const roleController =      require('../controllers/role.controller')
const authMiddleware =      require('../middleware/auth.middleware')

const router = new Router()

router.post('/role', [authMiddleware, roleController.preCreate], asyncHandler(roleController.create))
router.post('/role/add_user', [authMiddleware, roleController.preAddUser], asyncHandler(roleController.addUser))
router.get('/roles', [authMiddleware], asyncHandler(roleController.getAll))
router.get('/role/:id', [authMiddleware, roleController.preGetOne], asyncHandler(roleController.getOne))
router.patch('/role', [authMiddleware, roleController.preUpdate], asyncHandler(roleController.update))
router.delete('/role/:id', [authMiddleware, roleController.preDelete], asyncHandler(roleController.delete))

module.exports = router
