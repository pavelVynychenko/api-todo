FROM node:12-alpine

RUN npm install
RUN npm install -g sequelize-cli

#
# Configure user
#
ARG UID
ARG GID

RUN apk --no-cache add shadow \
    && usermod -u $UID node \
    && groupmod -g $GID node

USER node

EXPOSE 4000

#
# Do not stop container, and handle SIGTERM from docker stop or down
#
CMD node -e 'setInterval(() => {}, 60000); process.on("SIGTERM", () => { process.exit(0); });'