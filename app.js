const createError = require('http-errors');
const express = require('express');
const cors = require("cors");
const authRouter = require('./routes/auth.routes')
const usersRouter = require('./routes/user.routes')
const dealsRouter = require('./routes/deal.routes')
const postsRouter = require('./routes/post.routes')
const roleRouter = require('./routes/role.routes')

const PORT = process.env.PORT;

const app = express();

// view engine setup
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//testing middleware

var requestTime = function (req, res, next) {
  const time = Date.now();
  req.requestTime = new Date(time).toString();;
  next();
};
app.use(requestTime);
app.get('/health', function(req, res) {
  let responseText = `nice requested at ${req.requestTime}`
  res.send(responseText)
})

//testing end

app.use('/api', authRouter)
app.use('/api', usersRouter)
app.use('/api', dealsRouter)
app.use('/api', postsRouter)
app.use('/api', roleRouter)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json(err.message);
  console.log(err.details || err)
});

app.listen(PORT, () => {
  console.log( 'App was started at port ' + '\x1b[33m%s\x1b[0m', PORT)
})

module.exports = app;
