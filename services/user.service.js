const { User, Role } = require('../database/models');

class UserService {

    static getAll() {
        return User.findAll({ 
            include: [{
                model: Role,
                as: 'roles'
            }],
        })
    }

    static getOne(userId) {
        return User.findByPk(userId)
    }

    static update(id, name, email, password) {
        return User.update({
            name: name,
            email: email,
            password: password,
        },
        { where: { id: id } })
    }

    static delete(userId) {
        return User.destroy({
            where: {id: userId}
        })
    }
}

module.exports = UserService