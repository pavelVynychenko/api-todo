const { User, Role } = require('../database/models');
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const { Op } = require('sequelize');

class AuthService {
    static generateToken(payload) {
        return jwt.sign(payload, process.env.TOKEN_SECRET, { expiresIn: "8h" })
    }

    static isUserExists(email) {
        return User.count({ where: { email: email } })
    }

    static async registration({ name, email, password, roles }) {
        const hashPassword = await bcrypt.hash(password, 8)
        const userRoles = await Role.findAll({
            where: {
                roleName: {
                    [Op.or]: roles
                }
            }
        })
        const newUser = 
        await User.create({
            name: name,
            email: email,
            password: hashPassword,
        })
        newUser.setRoles(userRoles)

        const token = this.generateToken({id: newUser.id, email: newUser.email})
        return {
            token,
            user: {
                id: newUser.id,
                name: newUser.name,
                email: newUser.email,
                roles: userRoles
            },
            message: 'Registration completed'
        }
    }
    
    static auth(userId) {
        return User.findOne({ where: { id: userId } })
    }

    static async login(email, password) {
        const user = await User.findOne({ 
            where: { email: email },
            include: [{
                model: Role,
                as: 'roles'
            }],
        })
        user.passwordCheck(password)
        const token = this.generateToken({id: user.id, email: user.email})
        return {
            token,
            user: {
                id: user.id,
                name: user.name,
                email: user.email,
                roles: user.roles.map(role => role.roleName)
            }
        }
    }
}

module.exports = AuthService