const { Post, Comment } = require('../database/models');


class PostService {
    static getAll() {
        return Post.findAll({ 
            include: [{
                model: Comment,
                as: 'comments'
            }],
        })
    }
}

module.exports = PostService