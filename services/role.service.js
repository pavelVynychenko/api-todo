const { User, Role } = require('../database/models');

class RoleService {

    static getAll() {
        return Role.findAll({
            include: [{
              model: User,
              as: 'users'
            }],
          })
    }

    static getOne(roleId) {
        return Role.findByPk(roleId, {
            include: [{
              model: User,
              as: 'users'
            }],
        })
    }

    static create(roleName) {
        return Role.create({
            roleName: roleName,
        })
    }

    static isRoleExists(roleName) {
        return Role.count({ where: { roleName: roleName } })
    }

    static findRole(roleId) {
        return Role
        .findByPk(roleId, {
          include: [{
            model: User,
            as: 'users'
          }],
        })
    }

    static async update(id, roleName) {
        const role = await Role
        .findByPk(id, {
            include: [{
            model: User,
            as: 'users'
            }],
        })
        return role
        .update({
            roleName: roleName,
        })
    }

    static delete(id) {
        return Role.destroy({
            where: {id: id}
        })
    }
}

module.exports = RoleService