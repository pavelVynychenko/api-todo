const { Deal } = require('../database/models');


class DealService {
    static create(deal) {
        const { userId, title, content, status, priority } = deal
        return Deal.create({
            title: title,
            content: content,
            status: status,
            priority: priority,
            userId: userId,
        })
    }

    //todo: need to refactor filters part
    static getAll(userId, filters, pagination) {
        //filters scopes
        const fScopes = Object.entries(filters)
            .filter(([k, v]) => v)
            .map(([k,v]) => ({method: ['byKeyInArray', k, [v]]}))
        const searchScope = {method: ['searchable', 'title', [filters.title]]}
        //default methods
        const scopes = [
            {method: ['byKeyInArray', 'userId', [userId]]},
            {method: ['paginable', pagination.limit, pagination.offset]},
            {method: ['ordered', 'id', 'asc']},
            ...fScopes, searchScope
        ]
        return Deal
        .scope(...scopes)    
        .findAll({})
    }

    static getOne(dealId) {
        console.log('niiiice', dealId)
        return Deal.findByPk(dealId)
    }

    static async update(deal) {
        const { id, title, content, status, priority } = deal
        await Deal.update({
            title: title,
            content: content,
            status: status,
            priority: priority,
            },
            { where: { id: id } }
        )
        return Deal.findByPk(id)
    }

    static delete(dealId) {
        return Deal.destroy({
            where: {id: dealId}
        })
    }
}

module.exports = DealService